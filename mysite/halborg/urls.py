from django.conf.urls import url
from . import views

app_name = 'halborg'
urlpatterns = [
	url(r'^$', views.HomeInfo.as_view(), name='home'),
    url(r'^corporate/$', views.CorporatePage.as_view(), name='corporate_overview'),
    url(r'^inheritance_claims/$', views.InheritanceClaims.as_view(), name='inheritance'),
    url(r'^personal_injury/$', views.PersonalInjury.as_view(), name='personal_injury'),
    url(r'^negligence_claims/$', views.NegligenceClaims.as_view(), name='negligence_claims_overview'),
    url(r'^negligence_claims/claims_against_architects/$', views.ClaimsAgainstArchitects.as_view(), name='claims_against_architects'),
	url(r'^negligence_claims/claims_against_solicitors/$', views.SolicitorClaims.as_view(), name='claims_against_your_solicitor'),
    url(r'^employment/$', views.Employment.as_view(), name="employment"),
    url(r'^contact/$', views.ContactUs.as_view(), name="contact_us"),
    url(r'^corporate/commercial_contracts/$', views.CommercialContracts.as_view(), name='commercial_contracts'),
    url(r'^corporate/business_company_formation/$', views.BusinessAndCompany.as_view(), name='business_and_company_formation'),
    url(r'^personnel/$', views.Personnel.as_view(), name='personnel'),
    url(r'^corporate/business_purchases_sales/$', views.BusinessPurchases.as_view(), name='business_and_company_purchases_and_sales'),
    url(r'^success/$', views.success, name='success'),
    url(r'^corporate/partnerships_joint_ventures/$', views.PartnershipsJointVentures.as_view(), name='partnerships_and_joint_ventures'),
    url(r'^corporate/terms_and_conditions_of_business/$', views.BusinessTermsConditions.as_view(), name='terms_and_conditions_of_business'),
    url(r'^corporate/employment_contracts/$', views.EmploymentContracts.as_view(), name='employment_contracts'),
    url(r'^corporate/company_secretarial_services/$', views.CompanySecretarial.as_view(), name='company_secretarial_services'),
]
