# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView
from django.core.mail import send_mail, BadHeaderError
from halborg.forms import ContactForm
from django.views.generic.edit import FormView
from collections import OrderedDict
from django.views.generic.edit import CreateView


nav_dict = {"corporate": ['corporate_overview', 'commercial_contracts', 'business_and_company_formation',
            'business_and_company_purchases_and_sales', 'partnerships_and_joint_ventures',
	        'terms_and_conditions_of_business', 'employment_contracts', 'company_secretarial_services'],
            "negligence_claims": ['negligence_claims_overview', 'claims_against_architects', 'claims_against_your_solicitor']}


class TemplatePage(TemplateView):

	def get_context_data(self, **kwargs):
		context = super(TemplatePage, self).get_context_data(**kwargs)
		context['nav_dict'] = nav_dict
		return context

class CorporatePage(TemplatePage):
    template_name = "corporate.html"

    def get_context_data(self, **kwargs):
    	# put all variables into the context dictionary here
    	context = super(CorporatePage, self).get_context_data(**kwargs)
    	context['page'] = 'corporate_overview'
    	return context

class InheritanceClaims(TemplatePage):
    template_name = "inheritance_claims.html"

    def get_context_data(self, **kwargs):

        context = super(InheritanceClaims, self).get_context_data(**kwargs)
        context['page'] = 'inheritance'
        return context


class PersonalInjury(TemplatePage):
    template_name = "personal_injury.html"

    def get_context_data(self, **kwargs):

        context = super(PersonalInjury, self).get_context_data(**kwargs)

        context['page'] = 'personal_injury'
        return context

class NegligenceClaims(TemplatePage):
    template_name = "negligence_claims.html"

    def get_context_data(self, **kwargs):
        context = super(NegligenceClaims, self).get_context_data(**kwargs)

        context['page'] = 'negligence_claims_overview'
        return context


class ClaimsAgainstArchitects(TemplatePage):
	template_name = "negligence_claims.html"

	def get_context_data(self, **kwargs):
		context = super(ClaimsAgainstArchitects, self).get_context_data(**kwargs)
		context['page'] = 'claims_against_architects'
		return context

class HomeInfo(TemplatePage):
    template_name = "home_info.html"

    def get_context_data(self, **kwargs):
        context = super(HomeInfo, self).get_context_data(**kwargs)

        context['page'] = 'home'
        return context


class Employment(TemplatePage):
    template_name = "employment.html"

    def get_context_data(self, **kwargs):
        context = super(Employment, self).get_context_data(**kwargs)

        context['page'] = 'employment'
        return context

class CommercialContracts(TemplatePage):
    template_name = "commercial_contracts.html"

    def get_context_data(self, **kwargs):
        context = super(CommercialContracts, self).get_context_data(**kwargs)

        context['page'] = 'commercial_contracts'
        return context

class BusinessAndCompany(TemplatePage):
    template_name = "business_and_company.html"

    def get_context_data(self, **kwargs):
        context = super(BusinessAndCompany, self).get_context_data(**kwargs)

        context['page'] = 'business_and_company_formation'
        return context


class Personnel(TemplatePage):
    template_name = "personnel.html"

    def get_context_data(self, **kwargs):
        context = super(Personnel, self).get_context_data(**kwargs)

        context['page'] = 'personnel'
        return context


class SolicitorClaims(TemplatePage):
    template_name = "solicitor_claims.html"

    def get_context_data(self, **kwargs):
        context = super(SolicitorClaims, self).get_context_data(**kwargs)

        context['page'] = 'claims_against_your_solicitor'
        return context


class BusinessPurchases(TemplatePage):
    template_name = "business_purchases.html"

    def get_context_data(self, **kwargs):
        context = super(BusinessPurchases, self).get_context_data(**kwargs)

        context['page'] = 'business_and_company_purchases_and_sales'
        return context


class PartnershipsJointVentures(TemplatePage):
    template_name = "partnerships_joint_ventures.html"

    def get_context_data(self, **kwargs):
        context = super(PartnershipsJointVentures, self).get_context_data(**kwargs)

        context['page'] = 'partnerships_and_joint_ventures'
        return context


class BusinessTermsConditions(TemplatePage):
    template_name = "terms_and_conditions.html"

    def get_context_data(self, **kwargs):
        context = super(BusinessTermsConditions, self).get_context_data(**kwargs)

        context['page'] = 'terms_and_conditions_of_business'
        return context

class EmploymentContracts(TemplatePage):
    template_name = "employment_contracts.html"

    def get_context_data(self, **kwargs):
        context = super(EmploymentContracts, self).get_context_data(**kwargs)

        context['page'] = 'employment_contracts'
        return context


class CompanySecretarial(TemplatePage):
    template_name = "company_secretarial.html"

    def get_context_data(self, **kwargs):
        context = super(CompanySecretarial, self).get_context_data(**kwargs)

        context['page'] = 'company_secretarial_services'
        return context

class ContactUs(FormView, TemplatePage):
    template_name = "contact.html"
    form_class = ContactForm
    success_url = '/success/'

    # def email(request):
    #     if request.method == 'GET':
    #         form = ContactForm()
    #     else:
    #         form = ContactForm(request.POST)
    #         if form.is_valid():
    #             subject = form.cleaned_data['subject']
    #             from_email = form.cleaned_data['from_email']
    #             message = form.cleaned_data['message']
    #             try:
    #                 send_mail(subject, message, from_email, ['admin@example.com'])
    #             except BadHeaderError:
    #                 return HttpResponse('Invalid header found.')
    #             return redirect('success')
    #         return render(request, 'email.html', {'form': form})

    def get_context_data(self, **kwargs):
        context = super(ContactUs, self).get_context_data(**kwargs)

        context['page'] = 'contact_us'
        return context


def success(request):
    #write an if/else statement to replace the form section with the success message
    return render(request, 'success.html')
