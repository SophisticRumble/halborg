from django import template

register = template.Library()

@register.filter
def humanize(val): 
	return val.replace('_', ' ').replace('and', '&').title()

@register.filter
def get_dict_value(key, _dict): 

	if key in _dict: 
		return _dict[key]
	return '' 
